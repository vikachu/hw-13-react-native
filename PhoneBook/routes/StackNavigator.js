import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ContactInfo from '../components/ContactInfo';
import ContactList from '../components/ContactList';

const Stack = createStackNavigator();

function StackNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Contacts" component={ContactList} />
      <Stack.Screen name="ContactInfo" component={ContactInfo} />
    </Stack.Navigator>
  );
}

export default StackNavigator;
