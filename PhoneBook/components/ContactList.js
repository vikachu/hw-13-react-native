import React from 'react';
import {
  TextInput,
  Text,
  SafeAreaView,
  FlatList,
  StyleSheet,
  PermissionsAndroid,
  Button,
} from 'react-native';
import Contacts from 'react-native-contacts';

import ContactItem from './ContactItem';
import {TouchableOpacity} from 'react-native-gesture-handler';

class ContactList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contacts: [],
    };

    this.search = this.search.bind(this);
    this.onAddPress = this.onAddPress.bind(this);
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: 'Contacts',
        message: 'This application would like to access your contacts.',
      }).then(() => {
        this.loadContacts();
      });
    } else {
      this.loadContacts();
    }
  }

  loadContacts() {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        console.warn('Permission to access contacts was denied');
      } else {
        contacts.sort((a, b) => a.displayName > b.displayName);
        this.setState({contacts});
      }
    });
  }

  search(text) {
    if (text === '' || text === null) {
      this.loadContacts();
    } else {
      Contacts.getContactsMatchingString(text, (err, contacts) => {
        this.setState({contacts});
      });
    }
  }

  onAddPress() {
    Contacts.openContactForm({}, (err, contact) => {
      if (err) throw err;
      this.loadContacts();
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity onPress={this.onAddPress} style={styles.addButton}>
          <Text>Add contact</Text>
        </TouchableOpacity>
        <TextInput
          onChangeText={this.search}
          placeholder="🔍 Search"
          placeholderTextColor="black"
          style={styles.searchBar}
        />
        <FlatList
          data={this.state.contacts}
          renderItem={(contact) => {
            return (
              <ContactItem
                key={contact.item.recordID}
                item={contact.item}
                navigation={this.props.navigation}
              />
            );
          }}
          keyExtractor={(item) => item.recordID}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
  },
  searchBar: {
    backgroundColor: '#e0ebf5',
    borderRadius: 50,
    paddingHorizontal: 15,
    marginTop: 30,
    marginBottom: 30,
  },
  addButton: {
    borderRadius: 4,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    marginTop: 5,
  },
});

export default ContactList;
