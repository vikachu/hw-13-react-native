import React from 'react';
import call from 'react-native-phone-call';
import {Text, StyleSheet, View, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import Contacts from 'react-native-contacts';

class ContactInfo extends React.Component {
  onCallPress(args) {
    call(args).catch(console.error);
  }

  onDeleteCall(recordID) {
    // TODO: fix: delete doesn't work, contact remains
    Contacts.deleteContact({recordID: recordID}, (err, recordID) =>
      this.props.navigation.navigate('Contacts'),
    );
  }

  render() {
    const item = this.props.route.params.item;
    console.log(item);
    const number = item.phoneNumbers[0].number;
    return (
      <View>
        <View style={styles.avatarContainer}>
          {item.hasThumbnail ? (
            <Image source={{uri: item.thumbnailPath}} style={styles.avatar} />
          ) : (
            <View style={styles.avatarPlaceholder}>
              <Text style={styles.avatarPlaceholderText}>
                {item.givenName.charAt(0) + item.familyName.charAt(0)}
              </Text>
            </View>
          )}
        </View>

        <View style={styles.infoItemContainer}>
          <Text style={styles.label}>Name</Text>
          <Text style={styles.text}>{item.displayName}</Text>
        </View>

        <View style={styles.infoItemContainer}>
          <Text style={styles.label}>Phone number</Text>
          <Text style={styles.text}>{number}</Text>
        </View>

        <View style={styles.buttonsContainer}>
          <TouchableOpacity
            onPress={() => this.onCallPress({number: number, prompt: true})}
            style={styles.button}>
            <Text>Call</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.onDeleteCall(item.recordID)}
            style={styles.button}>
            <Text>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  avatarContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 4,
  },
  avatarPlaceholder: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 100,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: '#868282',
  },
  avatarPlaceholderText: {
    fontSize: 25,
    fontWeight: '900',
  },
  label: {
    fontSize: 12,
    marginBottom: 5,
    color: '#868282',
  },
  text: {
    color: '#3a3a3a',
    fontSize: 18,
  },
  infoItemContainer: {
    marginTop: 15,
    paddingHorizontal: 15,
  },
  buttonsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 30,
  },
  button: {
    borderRadius: 25,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    paddingVertical: 10,
    paddingHorizontal: 35,
  },
});

export default ContactInfo;
