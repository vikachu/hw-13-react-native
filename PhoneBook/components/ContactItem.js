import React from 'react';

import {Text, StyleSheet, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ContactItem = ({navigation, item}) => {
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('ContactInfo', {item})}
      style={styles.container}>
      <Text style={styles.text}>{item.displayName}</Text>
      <Text style={styles.email}>
        {item.emailAddresses.length !== 0 ? item.emailAddresses[0].email : ''}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 5,
    marginBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0ebf5',
  },
  text: {
    color: '#3a3a3a',
  },
  email: {
    color: '#868282',
    fontSize: 11,
  },
});

export default ContactItem;
